(function () {
    'use strict';

    function MainController($scope, $rootScope, $mdToast) {

        $rootScope.$on('$routeChangeSuccess', function (event, next) {

            $scope.isShowMenu = true;
            if (next.$$route && next.$$route.originalPath === '/login')
                $scope.isShowMenu = false;

        });


        $rootScope.toast = function (text, duration) {

            duration = duration || 4000;
            $mdToast.show(
                $mdToast.simple()
                    .textContent(text)
                    .position('top right')
                    .hideDelay(duration)
            );
        };

    }

    MainController.$inject = ['$scope', '$rootScope', '$mdToast'];

    angular.module('kbaseApp').controller('MainController', MainController);

}());