(function () {
    'use strict';

    // Module dependencies injection
    angular.module('kbaseApp', [
        'ngMaterial',
        'ngAnimate',
        'ngRoute',
        'angular.filter',
        'md.data.table'
    ])
        .config(function ($mdThemingProvider) {
            var customPrimary, customAccent, customWarn, customBackground;

            customPrimary = {
                '50': '#ffcc80',
                '100': '#ffc266',
                '200': '#ffb84d',
                '300': '#ffad33',
                '400': '#ffa31a',
                '500': '#FF9900',
                '600': '#e68a00',
                '700': '#cc7a00',
                '800': '#b36b00',
                '900': '#995c00',
                'A100': '#ffd699',
                'A200': '#ffe0b3',
                'A400': '#ffebcc',
                'A700': '#804d00'
            };

            customAccent = {
                '50': '#000000',
                '100': '#000000',
                '200': '#000000',
                '300': '#000000',
                '400': '#000000',
                '500': '#000000',
                '600': '#0d0d0d',
                '700': '#1a1a1a',
                '800': '#262626',
                '900': '#333333',
                'A100': '#0d0d0d',
                'A200': '#000000',
                'A400': '#000000',
                'A700': '#404040'
            };

            customWarn = {
                '50': '#ebbebe',
                '100': '#e5aaaa',
                '200': '#df9797',
                '300': '#d98383',
                '400': '#d36f6f',
                '500': '#cd5c5c',
                '600': '#c74848',
                '700': '#bc3a3a',
                '800': '#a93434',
                '900': '#952e2e',
                'A100': '#f1d1d1',
                'A200': '#f7e5e5',
                'A400': '#fdf8f8',
                'A700': '#822828'
            };
            customBackground = {
                '50': '#797274',
                '100': '#6b6668',
                '200': '#5e595b',
                '300': '#514d4e',
                '400': '#444042',
                '500': '#373435',
                '600': '#2a2828',
                '700': '#1d1b1c',
                '800': '#100f0f',
                '900': '#030202',
                'A100': '#857e81',
                'A200': '#928c8e',
                'A400': '#9e999b',
                'A700': '#000000'
            };

            $mdThemingProvider
                .definePalette('customWarn',
                customWarn);

            $mdThemingProvider
                .definePalette('customPrimary',
                customPrimary);

            $mdThemingProvider
                .definePalette('customAccent',
                customAccent);

            $mdThemingProvider
                .definePalette('customWarn',
                customAccent);

            $mdThemingProvider
                .definePalette('customBackground',
                customBackground);


            $mdThemingProvider.theme('default')
                .primaryPalette('customPrimary')
                .accentPalette('customAccent')
                .backgroundPalette('customBackground');
        });
}());


