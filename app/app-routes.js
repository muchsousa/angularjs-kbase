(function () {
    'use strict';

    angular.module('kbaseApp').config(['$routeProvider', '$locationProvider', ApplicationConfig]);

    function ApplicationConfig($routeProvider, $locationProvider) {

        $routeProvider

            .when('/home', {
                'templateUrl': 'app/components/home/home.html',
                'controller': 'HomeController'
            })

            .when('/list', {
                'templateUrl': 'app/components/list/list.html',
                'controller': 'ListController'
            })

            .when('/login', {
                'templateUrl': 'app/components/auth/login/login.html',
                'controller': 'LoginController'
            })

            .otherwise({
                'redirectTo': '/home'
            });

        $locationProvider.html5Mode(true);
    }
}());