(function () {

    'use strict';

    function LoginController($scope, authService, $location) {

        $scope.login = function (credential) {
            $scope.isLoading = true;
            $scope.errorMessage = null;

            credential.email = credential.email.toLowerCase();

            authService.login(credential, function (err) {
                $scope.isLoading = false;

                if (err)
                    $scope.errorMessage = err;
                else
                    $location.path('/home');
            });
        };

        $scope.showPassword = function (passwordFieldType) {

            if (passwordFieldType === 'password') {

                $scope.passwordFieldType = 'text';
                $scope.passwordVisibilityIcon = '_off';
            } else {

                $scope.passwordFieldType = 'password';
                $scope.passwordVisibilityIcon = '';
            }
        };

        $scope.forgotPassword = function () {
            return;
        };

        (function init() {
            $scope.passwordFieldType = 'password';
        }());

    }

    LoginController.$inject = ['$scope', 'authService', '$location'];

    angular.module('kbaseApp').controller('LoginController', LoginController);

}());
