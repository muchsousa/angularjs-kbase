(function () {

    'use strict';

    function HomeController($scope, httpService, peopleService, dogsService, catsService) {

        $scope.getPeople = function(perPage, page){
            return peopleService.get(perPage, page)
        };

        $scope.getDogs = function(perPage, page){
            return dogsService.get(perPage, page)
        };

        $scope.getCats = function(perPage, page){
            return catsService.get(perPage, page)
        };

    }

    HomeController.$inject = ['$scope','httpService','peopleService','dogsService','catsService'];
    angular.module('kbaseApp').controller('HomeController', HomeController);

}());
