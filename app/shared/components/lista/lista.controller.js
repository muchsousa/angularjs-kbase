(function () {
    'use strict';

    function ListaController() {

        var ctrl = this;
        
        this.pagination = {
                page: 1,
                perPage: 5,
                pages: null
            }

        this.items = []
        this.isLoading = true

        // requesita items da api
        var getItems = function () { 
            ctrl.isLoading = true

            ctrl.fn(ctrl.pagination.perPage, ctrl.pagination.page).then(function(res) {
                ctrl.items = res.items
                ctrl.isLoading = false
                ctrl.pagination.pages = res.pagination.pages
            })
        }

        // Busca os items da pagina 1
        getItems()
    
        // Volta para a Página anterior
        this.prevPage = function () {
            if(ctrl.pagination.page > 1)
                ctrl.pagination.page--

            getItems()
        }

        // Vai para a Próxima Página  
        this.nextPage = function () {
            if(ctrl.pagination.page < ctrl.pagination.pages)
                ctrl.pagination.page++

            getItems()
        }
     
        // Altera o numero de itens por Página
        this.changeItensPerPage = function () {
            getItems()
        }

    }
    
    angular.module('kbaseApp').controller('ListaController', ListaController)

}());
