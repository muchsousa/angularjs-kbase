(function () {
    'use strict';

    angular.module('kbaseApp').component('listaComponent', {
      templateUrl: '/app/shared/components/lista/lista.html',
      controller: 'ListaController',
      bindings: {
        fn: '='
      }
    });

}());
