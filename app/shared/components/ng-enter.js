(function () {
    'use strict';

    function EnterKeyPressDirective() {

        return function (scope, element, attrs) {

            element.bind('keydown keypress', function (event) {

                if (event.which === 13) {

                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    }

    angular.module('kbaseApp').directive('ngEnter', EnterKeyPressDirective);

}());
