(function () {

    'use strict';

    function AuthService(httpService, localstorageService) {

        const module = 'auth/';

        this.login = function (credential, cb) {
            const url = module + 'login';

            httpService.post(url, credential)
                .success(function (user, status, headers) {
                    localstorageService.set('KBASE-CREDENTIAL', user);

                    localstorageService.set('KBASE-TOKEN', headers('authorization'));

                    return cb(null, user);
                })
                .error(function (err) {
                    console.log(err);

                    if (!err)
                        return cb('Servidor não encontrado. Verifique sua conexão.');
                    if (err.statusCode === 404)
                        return cb('Usuário não encontrado.');
                    if (err.statusCode === 401)
                        return cb('Email ou senha incorreto.');

                    return cb('Erro desconhecido');
                });
        };

        this.logout = function (cb) {
            const url = module + 'logout';

            httpService.put(url)
                .success(function (hasDisconnected) {
                    if (hasDisconnected) {
                        localstorageService.delete('KBASE-CREDENTIAL');

                        localstorageService.delete('KBASE-TOKEN');
                    }

                    return cb(null, hasDisconnected);
                })
                .error(function (err) {
                    return cb(err);
                });
        };

    }

    AuthService.$inject = ['httpService', 'localstorageService'];

    angular.module('kbaseApp').service('authService', AuthService);

}());