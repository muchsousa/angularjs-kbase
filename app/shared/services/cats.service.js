(function () {

    'use strict';

    function catsService(httpService) {

        this.get = function (perPage, page) {
            return httpService.get('cats?perPage='+perPage+'&page='+page)
            .then(function(res){             

                var result = res.data.people

                // Percorre o array e coloca o caminho de uma foto aleatória em cada elemento
                for (var i = 0; i < result.length; i++){
                    result[i].image = 'https://image.flaticon.com/icons/svg/194/194623.svg'
                    result[i].description = result[i].address
                    result[i].address = null
                }
                
                return {
                    items: result,
                    pagination: {
                        pages: res.data.data.pages
                    }
                }

            })
        };

    }

    catsService.$inject = ['httpService'];

    angular.module('kbaseApp').service('catsService', catsService);

}());