(function () {

    'use strict';

    function CredentialsService(httpService, $q) {

        const module = 'credentials/';

        this.checkPassword = function (credential, cb) {
            const url = module + 'validate-password';

            httpService.post(url, credential)
                .success(function (isEqual) {
                    return cb(null, isEqual);
                })
                .error(function (err) {
                    return cb(err);
                });
        };

        this.updatePassword = function (credential, cb) {
            const url = module + 'update-password';

            httpService.put(url, credential)
                .success(function (isUpdated) {
                    return cb(null, isUpdated);
                })
                .error(function (err) {
                    return cb(err);
                });
        };

        this.changePassword = function (credential, cb) {
            const self = this,
                deferred = $q.defer(),

                newPassword = credential.newPassword;

            delete credential.id_credential;
            delete credential.newPassword;
            delete credential.pwdConfirm;

            self.checkPassword(credential, function (err) {
                if (err)
                    return deferred.reject(err);

                credential.password = newPassword;

                self.updatePassword(credential, function (err) {
                    if (err)
                        return deferred.reject(err);

                    return deferred.resolve();

                });
            });

            return deferred.promise.then(function () {
                return cb('Senha atualizada com sucesso');

            }, function (err) {
                if (err.statusCode === 401)
                    return cb('Senha atual está incorreta');

                return cb('Erro ao atualizar senha.');
            });

        };

    }

    CredentialsService.$inject = ['httpService', '$q'];

    angular.module('kbaseApp').service('credentialsService', CredentialsService);

}());