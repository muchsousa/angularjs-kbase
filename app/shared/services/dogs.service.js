(function () {

    'use strict';

    function dogsService(httpService) {

        this.get = function (perPage, page) {
            return httpService.get('dogs?perPage='+perPage+'&page='+page)
            .then(function(res){             

                var result = res.data.people

                // Percorre o array e coloca o caminho de uma foto aleatória em cada elemento
                for (var i = 0; i < result.length; i++){
                    result[i].image = 'https://banner2.kisspng.com/20180629/osc/kisspng-pet-sitting-puppy-dog-walking-dogs-for-good-dementia-5b364aa80fde58.445968431530284712065.jpg'
                    result[i].description = result[i].address
                    result[i].address = null
                }
                
                return {
                    items: result,
                    pagination: {
                        pages: res.data.data.pages
                    }
                }
            })
        };

    }

    dogsService.$inject = ['httpService'];

    angular.module('kbaseApp').service('dogsService', dogsService);

}());