(function () {

    'use strict';

    function HttpService($http, environmentConfig) {

        const kbaseServerUrl = environmentConfig.api;

        this.get = function (module, id) {
            var url = kbaseServerUrl + module;

            if (id)
                url = url + '' + id;

            return $http.get(url)
        };

        this.post = function (module, payload) {
            return $http.post(kbaseServerUrl + module, payload);
        };

        this.put = function (module, payload) {
            return $http.put(kbaseServerUrl + module, payload);
        };

        this.delete = function (module, id) {
            return $http.delete(kbaseServerUrl + module + id);
        };

    }

    HttpService.$inject = ['$http', 'environmentConfig'];

    angular.module('kbaseApp').service('httpService', HttpService);

}());