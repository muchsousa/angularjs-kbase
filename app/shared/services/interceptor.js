(function () {

    'use strict';

    angular.module('kbaseApp').config(['$httpProvider', Interceptor]);

    function Interceptor($httpProvider) {

        $httpProvider.interceptors.push(function ($q, localstorageService) {

            return {

                'request': function (config) {

                    config.headers.authorization = localstorageService.get('KBASE-TOKEN');
                    return config;
                },

                'responseError': function (error) {

                    return $q.reject(error);
                }
            };
        });
    }

}());