(function () {

    'use strict';

    function LocalStorageService($window) {

        this.get = function (key) {

            try {
                return JSON.parse($window.localStorage.getItem(key));
            } catch (e) {
                return null;
            }
        };

        this.set = function (key, value) {
            $window.localStorage.setItem(key, JSON.stringify(value));
        };

        this.delete = function (key) {
            $window.localStorage.removeItem(key);
        };
    }

    LocalStorageService.$inject = ['$window'];

    angular.module('kbaseApp').service('localstorageService', LocalStorageService);

}());