(function () {

    'use strict';

    function peopleService(httpService) {

        this.get = function (perPage, page) {
            return httpService.get('people?perPage='+perPage+'&page='+page)
            .then(function(res){             

                var result = res.data.people

                // Percorre o array e coloca o caminho de uma foto aleatória em cada elemento
                for (var i = 0; i < result.length; i++){
                    result[i].image = 'https://s3.amazonaws.com/eclincher.wp.upload/wp-content/uploads/2015/08/25155834/people-icon.png'
                    result[i].description = result[i].address
                    result[i].address = null
                }
                
                return {
                    items: result,
                    pagination: {
                        pages: res.data.data.pages
                    }
                }
            })
        };

    }

    peopleService.$inject = ['httpService'];

    angular.module('kbaseApp').service('peopleService', peopleService);

}());