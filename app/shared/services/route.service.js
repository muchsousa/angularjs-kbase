(function () {

    'use strict';

    function RouteService(httpService) {

        this.checkRoute = function (cb) {
            httpService.get('auth/check-route')
                .success(function () {
                    return cb(true);
                })
                .error(function () {
                    return cb(false);
                });
        };

    }

    RouteService.$inject = ['httpService'];

    angular.module('kbaseApp').service('routeService', RouteService);

}());