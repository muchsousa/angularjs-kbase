module.exports = { // eslint-disable-line

    /**
     * Deployment section
     * http://pm2.keymetrics.io/docs/usage/deployment/
     */

    'apps': [

        {
            'name': 'kbaseapp',
            'script': 'server.js',
            'env': {
                'COMMON_VARIABLE': 'true'
            },
            'env_production': {
                'NODE_ENV': 'production'
            }
        }],

    'deploy': {

        'hml': {
            'user': 'root',
            'host': '127.0.0.1',
            'ref': 'origin/hml',
            'repo': 'git@github.com:KbaseIT/kbase-app.git',
            'path': '/kbase/kbase-app/',
            'post-deploy': 'npm install && gulp build:app --env hml && pm2 reload ecosystem.config.js',
            'env': {
                'NODE_ENV': 'hml'
            }
        }
    }
};
