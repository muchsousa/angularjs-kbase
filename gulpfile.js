'use strict';

const gulp = require('gulp'),
    del = require('del'),
    eslint = require('gulp-eslint'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    connect = require('gulp-connect'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-clean-css'),
    gutil = require('gulp-util'),
    ngAnnotate = require('gulp-ng-annotate'),
    gulpNgConfig = require('gulp-ng-config'),
    minimist = require('minimist'),

    CONNECT_SERVER = {
        'port': 1313,
        'livereload': true,
        'fallback': 'index.html'
    },

    APP_PREFIX = 'kbase',
    SRC_CODE = ['./app/app-module.js', './app/**/*.js', './app/**/**/*.js', './app/shared/filters/*.js', './app/shared/components/*.js', './app/app-config.js'],
    ASSETS_PATH = ['assets/**/images/*.*', 'assets/**/icons/*.*'],
    WATCH_RELOAD = ['./app/**/*.*', './assets/**/*.*'],
    WATCH_RELOAD_TEMPLATE = ['index.html', 'conf.json', './app/**/*.html'],
    FOLDERS_TO_CLEAN = ['build', 'dist', 'release'],
    CONFIG_CLEAN = ['./app/app-config.js'],
    CSS_PATHS = ['./app/**/*.css', '/app/**/dialogs/*.css', './assets/css/*.css'],
    DIST_PATH = './dist/',
    APP_PATH = './app/',
    CSS_DIST_PATH = './dist/assets/css',
    ASSETS_DIST_PATH = './dist/assets/';

var environment = 'development',
    argv = minimist(process.argv.slice(2));

if (argv.env) {

    switch (argv.env) {
    case 'dev':
        environment = 'development';
        break;

    case 'hml':
        environment = 'homologation';
        break;

    case 'prod':
        environment = 'production';
        break;

    default:
        break;
    }

}
/**
 * Tasks principais
 */
gulp.task('test', ['eslint']);
gulp.task('default', ['debug']);
gulp.task('debug', ['build:general', 'connect', 'watch']);
gulp.task('build:general', ['build:envs', 'build:bundle', 'build:min', 'clean:config']);
gulp.task('build:assets', ['minify:css', 'copy:assets']);
gulp.task('build:app', ['build:general', 'build:assets']);


gulp.task('eslint', () => {
    gulp.src(['**/*.js','!node_modules/**','!dist/**'])
    .pipe(eslint({
        rules: {
            "no-irregular-whitespace": 1,
            "array-bracket-spacing": 1,
            "block-spacing": 2,
            "no-const-assign": 2,
            "indent": [2, 4]
        },
        globals: [
            '$'
        ],
        envs: [
            'browser'
        ]
    }))
    .pipe(eslint.formatEach('compact', process.stderr));
});

/**
 * Tasks de suporte
 */

// Deleta config
gulp.task('clean:temporary', () => {
    del(CONFIG_CLEAN);
    del(FOLDERS_TO_CLEAN);
});

// Injeta variaveis de configuracao na app
gulp.task('build:envs', ['clean:temporary'], function () {

    return gulp.src('config.json')
        .pipe(gulpNgConfig(APP_PREFIX + 'App', {
            'createModule': false,
            'environment': environment
        }))
        .pipe(concat('app-config.js'))
        .pipe(gulp.dest(APP_PATH));

});

// Unifica
gulp.task('build:bundle', ['clean:temporary', 'build:envs'], function () {

    return gulp.src(SRC_CODE)
        .pipe(ngAnnotate())
        .pipe(concat(APP_PREFIX + '.js'))
        .pipe(gulp.dest(DIST_PATH));
});

// Minifica JS
gulp.task('build:min', ['clean:temporary', 'build:envs', 'build:bundle'], () => {

    if (environment === 'development') {

        return gulp.src(SRC_CODE)
            .pipe(sourcemaps.init())
            .pipe(concat(APP_PREFIX + '.min.js'))
            .pipe(ngAnnotate())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(DIST_PATH));
    } else {

        return gulp.src(SRC_CODE)
            .pipe(sourcemaps.init())
            .pipe(concat(APP_PREFIX + '.min.js'))
            .pipe(ngAnnotate())
            .pipe(uglify({ 'mangle': false }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(DIST_PATH));

    }

});

// Deleta config e source da app
gulp.task('clean:config', ['clean:temporary', 'build:bundle', 'build:envs', 'build:min'], () => {

    del(CONFIG_CLEAN);    
});

// Minifica CSS
gulp.task('minify:css', () => {

    return gulp.src(CSS_PATHS)
        .pipe(sourcemaps.init())
        .pipe(concat(APP_PREFIX + '.min.css'))
        .pipe(minifyCss({
            'processImport': false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(gutil.env.src || CSS_DIST_PATH));
});

// Copia assets para dist
gulp.task('copy:assets', ['minify:css'], () => {

    return gulp.src(ASSETS_PATH).pipe(gulp.dest(ASSETS_DIST_PATH));
});

// Inicia Http Server
gulp.task('connect', ['build:app'], () => {

    connect.server(CONNECT_SERVER);
    gutil.log('-> Environment:', environment);
});

// Inicia watch na estrutura
gulp.task('watch', () => {

    gulp.watch(WATCH_RELOAD_TEMPLATE, ['reload:template']);
    gulp.watch(WATCH_RELOAD, ['reload']);
});

// Live Reload
gulp.task('reload', ['build:app'], () => {
    connect.reload();
});

// Recarrega CSS & HTML
gulp.task('reload:template', () => {
    connect.reload();
});