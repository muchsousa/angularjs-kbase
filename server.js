const port = (process.env.PORT || 8000), // eslint-disable-line
    directory = '.',
    express = require('express'),
    serveStatic = require('serve-static'),
    rewriteMiddleware = require('express-htaccess-middleware'),
    rewriteOptions = {
        'file': '.htaccess'
    },
    app = express();

// Habilita a reescrita de URL
app.use(rewriteMiddleware(rewriteOptions));

app.use(serveStatic(directory, {'index': ['index.html']}));

app.use((req, res) => {
    res.sendFile(`${__dirname}/index.html`); // eslint-disable-line
});

app.listen(port);

console.log("kbase -> " + port); // eslint-disable-line
exports = module.exports = app; // eslint-disable-line